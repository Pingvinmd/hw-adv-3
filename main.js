// Деструктуризація - це процес розбиття об'єкту або масиву на окремі змінні, що містять його властивості або елементи
//завдання 1
const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsTotal = [...new Set([...clients1, ...clients2])];

console.log(clientsTotal);
//Завдання 2
const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

const charactersShortInfo = characters.map(({ name, lastName, age }) => {
  const objectElement = { name, lastName, age };
  return objectElement;
});

console.log(charactersShortInfo);
//Завдання 3
const user1 = {
  name: "John",
  years: 30,
  //   isAdmin: true,
};


function createNewUser({ name, years, isAdmin = false }) {
  const user = {
    name,
    years,
    isAdmin,
  };
  return user;
}
const newUser = createNewUser(user1);
console.log(newUser);
// document.write(`Object user = ${JSON.stringify(newUser)} `);
// document.write(`Object user = ${Object.entries(newUser)}`);
document.write(
  `name: ${newUser.name}, years: ${newUser.years}, isAdmin: ${newUser.isAdmin}.`
);
// Завдання 4
const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(fullProfile);

const abjArr = Object.entries(fullProfile); // отримаю колекцію масивів

document.write('Досье о возможной личности Сатоши Накамото <ol start="1">');
abjArr.forEach(([key, value]) => {
  // перевірка типу властивості об'єкта
  if (typeof value == "object") {
    // претворюю об'єкт в JSON строку;
    value = JSON.stringify(value)
      // видалити {} з строки;
      .slice(1, -1)
      //додати пробіл після ":" та "," в строку;
      .replace(/:/g, ": ")
      .replace(/,/g, ", ");
    document.write(`<li>${key} - ${value};</li>`);
  } else {
    document.write(`<li>${key} - ${value};</li>`);
  }
});
document.write("<ol>");
// Завдання 5
const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

// const bookToAdd1 = {
//   name: "Game of thrones1",
//   author: "George R. R. Martin1",
// };

// const bookToAdd2 = {
//   name: "Game of thrones2",
//   author: "George R. R. Martin2",
// };

function addBook(element, ...otherBooks) {
  return [...element, ...otherBooks];
}

console.log(addBook(books, bookToAdd));
// Завдання 6
const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

function createObject({ name, surname, age = 41, salary = 55000 }) {
  const user = {
    name: name,
    surname: surname,
    age: age,
    salary: salary,
  };
  return user;
}
console.log(createObject(employee));
// Завдання 7
const array = ["value", () => "showValue"];

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'

// console.log(showValue); // () => "showValue"